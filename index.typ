// Some definitions presupposed by pandoc's typst output.
#let blockquote(body) = [
  #set text( size: 0.92em )
  #block(inset: (left: 1.5em, top: 0.2em, bottom: 0.2em))[#body]
]

#let horizontalrule = [
  #line(start: (25%,0%), end: (75%,0%))
]

#let endnote(num, contents) = [
  #stack(dir: ltr, spacing: 3pt, super[#num], contents)
]

#show terms: it => {
  it.children
    .map(child => [
      #strong[#child.term]
      #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
      ])
    .join()
}

// Some quarto-specific definitions.

#show raw.where(block: true): block.with(
    fill: luma(230), 
    width: 100%, 
    inset: 8pt, 
    radius: 2pt
  )

#let block_with_new_content(old_block, new_content) = {
  let d = (:)
  let fields = old_block.fields()
  fields.remove("body")
  if fields.at("below", default: none) != none {
    // TODO: this is a hack because below is a "synthesized element"
    // according to the experts in the typst discord...
    fields.below = fields.below.amount
  }
  return block.with(..fields)(new_content)
}

#let empty(v) = {
  if type(v) == "string" {
    // two dollar signs here because we're technically inside
    // a Pandoc template :grimace:
    v.matches(regex("^\\s*$")).at(0, default: none) != none
  } else if type(v) == "content" {
    if v.at("text", default: none) != none {
      return empty(v.text)
    }
    for child in v.at("children", default: ()) {
      if not empty(child) {
        return false
      }
    }
    return true
  }

}

#show figure: it => {
  if type(it.kind) != "string" {
    return it
  }
  let kind_match = it.kind.matches(regex("^quarto-callout-(.*)")).at(0, default: none)
  if kind_match == none {
    return it
  }
  let kind = kind_match.captures.at(0, default: "other")
  kind = upper(kind.first()) + kind.slice(1)
  // now we pull apart the callout and reassemble it with the crossref name and counter

  // when we cleanup pandoc's emitted code to avoid spaces this will have to change
  let old_callout = it.body.children.at(1).body.children.at(1)
  let old_title_block = old_callout.body.children.at(0)
  let old_title = old_title_block.body.body.children.at(2)

  // TODO use custom separator if available
  let new_title = if empty(old_title) {
    [#kind #it.counter.display()]
  } else {
    [#kind #it.counter.display(): #old_title]
  }

  let new_title_block = block_with_new_content(
    old_title_block, 
    block_with_new_content(
      old_title_block.body, 
      old_title_block.body.body.children.at(0) +
      old_title_block.body.body.children.at(1) +
      new_title))

  block_with_new_content(old_callout,
    new_title_block +
    old_callout.body.children.at(1))
}

#show ref: it => locate(loc => {
  let target = query(it.target, loc).first()
  if it.at("supplement", default: none) == none {
    it
    return
  }

  let sup = it.supplement.text.matches(regex("^45127368-afa1-446a-820f-fc64c546b2c5%(.*)")).at(0, default: none)
  if sup != none {
    let parent_id = sup.captures.first()
    let parent_figure = query(label(parent_id), loc).first()
    let parent_location = parent_figure.location()

    let counters = numbering(
      parent_figure.at("numbering"), 
      ..parent_figure.at("counter").at(parent_location))
      
    let subcounter = numbering(
      target.at("numbering"),
      ..target.at("counter").at(target.location()))
    
    // NOTE there's a nonbreaking space in the block below
    link(target.location(), [#parent_figure.at("supplement") #counters#subcounter])
  } else {
    it
  }
})

// 2023-10-09: #fa-icon("fa-info") is not working, so we'll eval "#fa-info()" instead
#let callout(body: [], title: "Callout", background_color: rgb("#dddddd"), icon: none, icon_color: black) = {
  block(
    breakable: false, 
    fill: background_color, 
    stroke: (paint: icon_color, thickness: 0.5pt, cap: "round"), 
    width: 100%, 
    radius: 2pt,
    block(
      inset: 1pt,
      width: 100%, 
      below: 0pt, 
      block(
        fill: background_color, 
        width: 100%, 
        inset: 8pt)[#text(icon_color, weight: 900)[#icon] #title]) +
      block(
        inset: 1pt, 
        width: 100%, 
        block(fill: white, width: 100%, inset: 8pt, body)))
}



#let article(
  title: none,
  authors: none,
  date: none,
  abstract: none,
  cols: 1,
  margin: (x: 1.25in, y: 1.25in),
  paper: "us-letter",
  lang: "en",
  region: "US",
  font: (),
  fontsize: 11pt,
  sectionnumbering: none,
  toc: false,
  toc_title: none,
  toc_depth: none,
  doc,
) = {
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
  )
  set par(justify: true)
  set text(lang: lang,
           region: region,
           font: font,
           size: fontsize)
  set heading(numbering: sectionnumbering)

  if title != none {
    align(center)[#block(inset: 2em)[
      #text(weight: "bold", size: 1.5em)[#title]
    ]]
  }

  if authors != none {
    let count = authors.len()
    let ncols = calc.min(count, 3)
    grid(
      columns: (1fr,) * ncols,
      row-gutter: 1.5em,
      ..authors.map(author =>
          align(center)[
            #author.name \
            #author.affiliation \
            #author.email
          ]
      )
    )
  }

  if date != none {
    align(center)[#block(inset: 1em)[
      #date
    ]]
  }

  if abstract != none {
    block(inset: 2em)[
    #text(weight: "semibold")[Abstract] #h(1em) #abstract
    ]
  }

  if toc {
    let title = if toc_title == none {
      auto
    } else {
      toc_title
    }
    block(above: 0em, below: 2em)[
    #outline(
      title: toc_title,
      depth: toc_depth
    );
    ]
  }

  if cols == 1 {
    doc
  } else {
    columns(cols, doc)
  }
}
#show: doc => article(
  title: [EbM at the medical doctor’s wrist – a proof of concept],
  authors: (
    ( name: [Wolf Jonas Friederichs],
      affiliation: [RWTH Aachen, Fakultät für Maschinenwesen, Aachen, Deutschland.],
      email: [] ),
    ( name: [Hendrik Friederichs],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [hendrik.friederichs\@uni-bielefeld.de] ),
    ),
  date: [2024-01-30],
  lang: "en",
  abstract: [#strong[Background / Hintergrund];: Maecenas turpis velit, ultricies non elementum vel, luctus nec nunc. Nulla a diam interdum, faucibus sapien viverra, finibus metus. Donec non tortor diam. In ut elit aliquet, bibendum sem et, aliquam tortor. Donec congue, sem at rhoncus ultrices, nunc augue cursus erat, quis porttitor mauris libero ut ex. Nullam quis leo urna. Donec faucibus ligula eget pellentesque interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean rhoncus interdum erat ut ultricies. Aenean tempus ex non elit suscipit, quis dignissim enim efficitur. Proin laoreet enim massa, vitae laoreet nulla mollis quis.

#strong[Methods / Methoden];: …

#strong[Results / Ergebnisse];: …

#strong[Conclusio / Schlussfolgerungen];: …

],
  toc_title: [Table of contents],
  toc_depth: 3,
  cols: 1,
  doc,
)
#import "@preview/fontawesome:0.1.0": *


#super[1] Universität Bielefeld, Medizinische Fakultät OWL \
#super[2] RWTH Aachen, Fakultät für Maschinenwesen, Aachen, Deutschland.

#super[✉] Correspondence: #link("mailto:hendrik.friederichs@uni-bielefeld.de")[Hendrik Friederichs \<hendrik.friederichs\@uni-bielefeld.de\>]

#block[
#callout(
body: 
[
This manuscript is a work in progress. However, thank you for your interest. Please feel free to visit this web site again at a later date.

#emph[Dieses Manuskript ist noch in Arbeit. Wir danken Ihnen jedoch für Ihr Interesse. Bitte besuchen Sie diese Website zu einem späteren Zeitpunkt noch einmal …]

]
, 
title: 
[
IN PROGRESS …
]
, 
background_color: 
rgb("#ffe5d0")
, 
icon_color: 
rgb("#FC5300")
, 
icon: 
fa-fire()
, 
)
]
#block[
#callout(
body: 
[
#strong[Relevantes Problem:] Evidence-Based Medicine \(EBM) bietet einen strukturierten Ansatz, um neueste Forschungsergebnisse in klinische Entscheidungen einzubeziehen, wobei individuelle Patientenpräferenzen berücksichtigt werden. Ihre Implementierung ist entscheidend für eine qualitativ hochwertige Patientenversorgung, allerdings bleibt die Integration von EBM in den klinischen Alltag eine Herausforderung. \
#strong[Fokussiertes Problem:] Die konsequente Anwendung von EBM in der Praxis stößt auf Schwierigkeiten. Beispielsweise zeigen Studien in verschiedenen medizinischen Bereichen eine zögerliche Integration effektiver Innovationen. Häufige Probleme umfassen Zeitmangel und eine langsame Etablierung neuer Erkenntnisse. Jünge Ärzte stehen vor der Herausforderung, relevante Fragen in der Patientenversorgung effektiv zu recherchieren. Dabei sind Zeitmangel und das Vergessen der klinischen Fragestellung Hauptgründe für eine unzureichende Recherche. Zudem stellt insbesondere in nicht-englischsprachigen Ländern das Verständnis englischer Fachliteratur eine Hürde dar. Auch die Ausbildung in EBM an medizinischen Fakultäten ist verbesserungswürdig, da nur klinisch integrierte Kurse einen positiven Effekt auf EBM-Kompetenzen zeigen. \
#strong[Gap des Problems:] Es existiert eine Lücke zwischen dem theoretischen Ideal von EBM und dessen praktischer Umsetzung. Der Wissenstransfer über Fachzeitschriften und Leitlinien reicht nicht aus, um eine effektive Anwendung von EBM in der täglichen Praxis zu gewährleisten. \
#strong[Lösung?:] Die Nutzung mobiler Geräte und spezieller Webseiten könnte helfen, Zeitmangel und Zugangsprobleme zu lösen. Trotz der Verfügbarkeit digitaler Ressourcen gibt es jedoch keine überzeugenden Beweise dafür, dass Lehrinterventionen zur Nutzung digitaler medizinischer Daten einen Einfluss auf die klinische Praxis haben. Die Studie zielt darauf ab, eine mobile App zu entwickeln, die den Zugang zu medizinischem Wissen im klinischen Alltag erleichtert. Die App, die für die Apple Watch konzipiert ist, ermöglicht es, über Sprachbefehle schnell und intuitiv auf medizinische Forschungsergebnisse zuzugreifen und soll die Effizienz für Mediziner erhöhen. \
#strong[Forschungsfragen:] Daher wurde eine Studie durchgeführt, um … \
#strong[Studienpopulation:] … \
#strong[Studiendesign:] … \
#strong[Datenerhebung:] … \
#strong[Ergebnisparameter:] Anzahl der richtigen Antworten insgesamt. \
#strong[Statistik:] Bestimmung der Prozentwerte für die absolute und relative Bewertung der Leistungen.

]
, 
title: 
[
STRUKTUR DES MANUSKRIPTS
]
, 
background_color: 
rgb("#ccf1e3")
, 
icon_color: 
rgb("#00A047")
, 
icon: 
fa-lightbulb()
, 
)
]
== Background
<background>
=== Broad problem
<broad-problem>
Evidence-Based Medicine \(EBM) provides a structured approach for integrating the latest research findings into clinical decision-making. This integration is fundamental for enhancing patient care.Sackett \[1\] described in his seminal article the necessity to align all available evidence directly to the individual patient’s context. Djulbegovic and Guyatt \(2017) emphasize that EBM grounds medical practice in solid scientific research, considering patient preferences and values in decision-making \[2\]. Therefore, the implementation of EBM in clinical practice is crucial for ensuring high-quality patient care.

=== Theoretical and/or empirical focus of the problem
<theoretical-andor-empirical-focus-of-the-problem>
While EBM is widely recognized, its consistent application in clinical practice remains challenging. Research integration into clinical practice is slow \[3\]. For instance, in plastic and reconstructive surgery, surgeons often over-prescribe antibiotic prophylaxis despite evidence for specific indications and durations \[4\]. Integrating even clearly effective innovations into daily clinical practice proves difficult \[5\].

Green and colleagues interviewed junior doctors about emerging questions during patient contacts \[6\]. They found two new questions per three patients, mainly regarding therapeutic \(38%) or diagnostic \(27%) issues, with an average of 0.7 questions per contact. Although junior doctors deemed 70% as crucial for patient treatment, only 29% of these questions were pursued. Similar investigation rates were found by Gorman \(14 % on the same day and of 30 % within five days) \[7\] and Covell \(30 % within the same day) \[8\]. The primary reasons for not pursuing questions were lack of time \(60%) and forgetting the question \(29%) \[9\].

Thus, several problems occur in daily clinical practice:

==== Time
<time>
A systematic review identified time constraint as the primary barrier for junior doctors practicing evidence-based medicine \(EBM) \[10\]. In clinical practice, medical searches must be concise. Research indicates that experienced researchers require 45 minutes to locate all relevant original articles \[11\]. Sackett and Straus report that doctors on ward rounds typically spend about 90 seconds per search \[12\].

==== Search
<search>
The initial step in evidence-based decision-making involves transforming medical information into specific clinical questions. On average, searches take 15 minutes, primarily utilizing textbooks \(31%) and original articles \(21%) \(QUELLE?). Ho and colleagues noted that only 20% of junior doctors find implementing EBM straightforward \[13\], while Montori et al.~identified \(non) existing expertise as a key issue \[14\].

==== Language
<language>
Language barriers, especially in reading and understanding English-language original articles, are significant in countries like Germany, Hungary, and Poland, more so than in the Netherlands or Switzerland \[15\]. This barrier also affects Japanese and other evidence-based medicine teachers \[16\]; \[17\].

==== Teaching
<teaching>
Bridging the gap between EBM and clinical practice may be achieved through targeted education and training programs. Despite efforts, no positive effect of post-graduate EBM training on medical education has been proven as of mid-2010 \[18\]; \[19\]. Clinically integrated courses are known to positively affect EBM skills, attitudes, and behavior \[20\]. Critical appraisal teaching, a cornerstone of EBM, involves evaluating and interpreting evidence as well as a systematic integration of the validity, the results and the relevance of the study for the own individual work. Yet only one high-quality study \[21\] demonstrated its positive impact on doctors’ skills and knowledge \[22\].

Effective strategies are needed to enhance EBM application in daily practice. Mimura et al.~\(2018) emphasized the importance of developing and disseminating EBM-based clinical practice guidelines \[23\]. However, guideline knowledge among physicians does not necessarily translate into better practice, as shown in a study of 2,500 general practitioners \[24\]. Farmer et al.~found that distributing research results and guidelines has a minimal positive impact on clinical work \[25\].

Identifying barriers and facilitators for integrating EBM into post-graduate training is crucial. A survey among EBM-teachers highlighted time constraints as a major obstacle \[15\]. Green and Ruff suggest mobile devices and specially designed web pages to aid junior doctors in their research \[6\]. However, there is no convincing evidence that teaching interventions regarding digital medical data access affect clinical practice or patient outcomes \[26\]. Consequently, Green and Ruff propose the use of mobile devices to solve the problems of lack of time and of computer access. They also recommend specially designed web pages supporting junior doctors in their search \[6\]. Probably the access to the global knowledge is the most cost-effective and accessible strategy to improve healthcare \[27\].

=== Focused problem statement
<focused-problem-statement>
The discrepancy between the theoretical ideal and practical implementation of EBM is evident. Despite EBM’s potential to improve healthcare, its consistent application is not always realized. The best way to enhance evidence-based behavior in clinical practice is through the integration of EBM into clinical workflows \[28\]. Technological advancements have significantly improved access to digital data. Up to now, access to the internet has spread markedly and led to a much better accessibility to global knowledge. Also, technical development increased the options to access digital data drastically. For example, it is possible to search medical databases via the internet and mobile devices within seconds. Original works increasingly are read directly or downloaded, too. For instance, PubMed publishes annually 40,000 studies \(2010), with a rising trend. Databases like the Cochrane Library and ACP Journal Club, and various clinical guideline portals, focus on clinical issues and aid in treatment recommendations.

=== Statement of study intent
<statement-of-study-intent>
We propose developing a mobile device application to facilitate access to medical knowledge in everyday clinical settings. This application will transform the Apple Watch into a sophisticated portal for medical knowledge, leveraging PubMed’s extensive database. It will enable users to quickly access medical information through voice commands, simplifying complex data retrieval. This tool aims to enhance efficiency for medical professionals and researchers by providing instantaneous and intuitive access to medical research.

The application encompasses the following components:

+ #strong[Voice Command for Query Input] Users can initiate searches through voice commands, streamlining the information retrieval process.

+ #strong[PubMed Search] PubMed, the most respected source for medical literature, serves as the primary database. The challenge lies in effectively filtering search results to capture the most relevant and recent studies.

+ #strong[Abstract Summarization via TLDRs from SemanticScholar or GPT-4 Large Language Model] Summaries generated by TLDRs from SemanticScholar or GPT-4 must be meticulously verified for accuracy. This ensures that the condensed information remains reliable and useful for medical professionals.

We performed a study of medical students to investigate the following questions:

+ What is …
+ Why are …

== Methods
<methods>
=== Setting and subjects
<setting-and-subjects>
…

=== Study design
<study-design>
…

=== Ethical approval
<ethical-approval>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sagittis posuere ligula sit amet lacinia. Duis dignissim pellentesque magna, rhoncus congue sapien finibus mollis. Ut eu sem laoreet, vehicula ipsum in, convallis erat. Vestibulum magna sem, blandit pulvinar augue sit amet, auctor malesuada sapien. Nullam faucibus leo eget eros hendrerit, non laoreet ipsum lacinia. Curabitur cursus diam elit, non tempus ante volutpat a. Quisque hendrerit blandit purus non fringilla. Integer sit amet elit viverra ante dapibus semper. Vestibulum viverra rutrum enim, at luctus enim posuere eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

=== Data collection
<data-collection>
Data collection for this study was determined à priori as follows:

…

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

=== Outcome Measures
<outcome-measures>
…

=== Statistical methods
<statistical-methods>
…

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # Download the dataset

# Read the data
penguins = read.csv("penguins.csv")

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # Build a scatterplot
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species),
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4"))
```

== Results
<results>
=== Recruitment Process and Demographic Characteristics
<recruitment-process-and-demographic-characteristics>
The recruitment process is shown in Figure 1. We obtained XX complete data sets \(return rate YY.Z%) after contacting …

=== Primary and secondary Outcomes
<primary-and-secondary-outcomes>
#figure([
#box(width: 1682.0pt, image("Durchschnittswerte_Selbsteinschätzung_NKLM_14a.jpg"))
], caption: figure.caption(
position: bottom, 
[
Beispielgrafik: ein Bild sagt mehr als tausend Worte …
]), 
kind: "quarto-float-fig", 
supplement: "Figure", 
)


== Discussion
<discussion>
=== Summary
<summary>
After the evaluation of all datasets, the following findings emerged. The first is that …

=== Limitation: study population
<limitation-study-population>
Vestibulum ultrices, tortor at mattis porta, odio nisi rutrum nulla, sit amet tincidunt eros quam facilisis tellus. Fusce eleifend lectus in elementum lacinia. Nam auctor nunc in massa ullamcorper, sit amet auctor ante accumsan. Nam ut varius metus. Curabitur eget tristique leo. Cras finibus euismod erat eget elementum. Integer vel placerat ex. Ut id eros quis lectus lacinia venenatis hendrerit vel ante.

=== Limitation: study design
<limitation-study-design>
…

=== Integration with prior work
<integration-with-prior-work>
According to Sepucha et al., 49 % of breast cancer patients named the flood of information as a communication barrier for doctor-patient-consultations \[29\]. In this study by Sepucha, 14 % of the patients expected not to tell their doctor their concerns during the next consultation \(genauer-Lesen!) because they were afraid of their doctor not knowing the answer \[29\].

On annual average, physicians affiliated to the German public health care system treat the internationally still very high number of 34 patients per day. To implement all recommended measures for an appropriate prevention and care of his chronically ill patients, a general practitioner would need about 18 hours daily. Partly for this reason, on average, less than half of an evidence-based care is currently realized \(Gesundheitsweise 2009 – QUELLE).

On the other hand, junior doctors consider the discussion with the patients about existing evidence and the integration of the patient’s opinion into the treatment decision a preferred style of work \[14\].

=== Implications for practice
<implications-for-practice>
Freddi und Román-Pumar \(2011) diskutieren, wie EBM manchmal als Einschränkung der klinischen Freiheit und als Hemmnis für Innovationen wahrgenommen wird \[30\]. Diese Wahrnehmung kann die Umsetzung von EBM in der Praxis beeinträchtigen und unterstreicht die Notwendigkeit, diese Lücke zu schließen.

=== Implications for research
<implications-for-research>
Praesent ornare dolor turpis, sed tincidunt nisl pretium eget. Curabitur sed iaculis ex, vitae tristique sapien. Quisque nec ex dolor. Quisque ut nisl a libero egestas molestie. Nulla vel porta nulla. Phasellus id pretium arcu. Etiam sed mi pellentesque nibh scelerisque elementum sed at urna. Ut congue molestie nibh, sit amet pretium ligula consectetur eu. Integer consectetur augue justo, at placerat erat posuere at. Ut elementum urna lectus, vitae bibendum neque pulvinar quis. Suspendisse vulputate cursus eros id maximus. Duis pulvinar facilisis massa, et condimentum est viverra congue. Curabitur ornare convallis nisl. Morbi dictum scelerisque turpis quis pellentesque. Etiam lectus risus, luctus lobortis risus ut, rutrum vulputate justo. Nulla facilisi.

=== Conclusions
<conclusions>
…

#block[
#heading(
level: 
2
, 
numbering: 
none
, 
[
References
]
)
]
#block[
#block[
1. Sackett DL, Rosenberg WM, Gray JM, Haynes RB, Richardson WS. Evidence based medicine: What it is and what it isn’t. Bmj. 1996;312:71–2.

] <ref-sackett1996evidence>
#block[
2. Djulbegovic B, Guyatt GH. #link("https://doi.org/10.1016/s0140-6736(16)31592-6")[Progress in evidence-based medicine: a quarter century on];. The Lancet. 2017;390:415–23.

] <ref-djulbegovic2017>
#block[
3. Berwick DM. Disseminating innovations in health care. Jama. 2003;289:1969–75.

] <ref-berwick2003disseminating>
#block[
4. Klifto KM, Rydz AC, Biswas S, Hultman CS, Erdmann D, Phillips BT. #link("https://doi.org/10.1097/prs.0000000000010608")[Evidence-Based Medicine: Systemic Perioperative Antibiotic Prophylaxis for Prevention of Surgical-Site Infections in Plastic and Reconstructive Surgery];. Plastic & Reconstructive Surgery. 2023;152:1154e–1182e.

] <ref-klifto2023>
#block[
5. Hayward RA, Asch SM, Hogan MM, Hofer TP, Kerr EA. Sins of omission: Getting too little medical care may be the greatest threat to patient safety. Journal of general internal medicine. 2005;20:686–91.

] <ref-hayward2005sins>
#block[
6. Green ML, Ruff TR. Why do residents fail to answer their clinical questions? A qualitative study of barriers to practicing evidence-based medicine. Academic medicine. 2005;80:176–82.

] <ref-green2005residents>
#block[
7. Gorman PN, Helfand M. Information seeking in primary care: How physicians choose which clinical questions to pursue and which to leave unanswered. Medical Decision Making. 1995;15:113–9.

] <ref-gorman1995information>
#block[
8. Covell DG, Uman GC, Manning PR. Information needs in office practice: Are they being met? Annals of internal medicine. 1985;103:596–9.

] <ref-covell1985information>
#block[
9. Green ML, Ciampi MA, Ellis PJ. Residents’ medical information needs in clinic: Are they being met? The American journal of medicine. 2000;109:218–23.

] <ref-green2000residents>
#block[
10. Dijk N van, Hooft L, Wieringa-de Waard M. What are the barriers to residents’ practicing evidence-based medicine? A systematic review. Academic Medicine. 2010;85:1163–70.

] <ref-van2010barriers>
#block[
11. Gorman PN, Ash J, Wykoff L. Can primary care physicians’ questions be answered using the medical journal literature? Bulletin of the Medical Library Association. 1994;82:140.

] <ref-gorman1994can>
#block[
12. Sackett DL, Straus SE, et al. Finding and applying evidence during clinical rounds: The evidence cart. Jama. 1998;280:1336–8.

] <ref-sackett1998finding>
#block[
13. Ho P, Kim P, Hong J, Fontelo P. Virtual evidence cart and resident use of EBM. In: JOURNAL OF GENERAL INTERNAL MEDICINE. SPRINGER 233 SPRING STREET, NEW YORK, NY 10013 USA; 2006. p. 161–1.

] <ref-ho2006virtual>
#block[
14. Montori VM, Tabini CC, Ebbert JO. A qualitative assessment of 1st-year internal medicine residents’ perceptions of evidence-based clinical decision making. Teaching and learning in medicine. 2002;14:114–8.

] <ref-montori2002qualitative>
#block[
15. Oude Rengerink K, Thangaratinam S, Barnfield G, Suter K, Horvath AR, Walczak J, et al. How can we teach EBM in clinical practice? An analysis of barriers to implementation of on-the-job EBM teaching and learning. Medical teacher. 2011;33:e125–30.

] <ref-oude2011can>
#block[
16. Matsui K, Ban N, Fukuhara S, Shimbo T, Koyama H, Nakamura S, et al. Poor english skills as a barrier for japanese health care professionals in learning and practising evidence-based medicine. Medical education. 2004;38:1204.

] <ref-matsui2004poor>
#block[
17. Letelier L, Zamarin N, Andrade M, Gabrielli L, Caiozzi G, Viviani P, et al. Exploring language barriers to evidence-based health care \(EBHC) in post-graduate medical students: A randomised trial. Educ Health. 2007;20:82–2.

] <ref-letelier2007exploring>
#block[
18. Feldstein DA, Maenner MJ, Srisurichan R, Roach MA, Vogelman BS. Evidence-based medicine training during residency: A randomized controlled trial of efficacy. BMC medical education. 2010;10:1–7.

] <ref-feldstein2010evidence>
#block[
19. Flores-Mateo G, Argimon JM. Evidence based practice in postgraduate healthcare education: A systematic review. BMC health services research. 2007;7:1–8.

] <ref-flores2007evidence>
#block[
20. Coomarasamy A, Khan KS. What is the evidence that postgraduate teaching in evidence based medicine changes anything? A systematic review. Bmj. 2004;329:1017.

] <ref-coomarasamy2004evidence>
#block[
21. Linzer M, Brown JT, Frazier LM, DeLong ER, Siegel WC. Impact of a medical journal club on house-staff reading habits, knowledge, and critical appraisal skills: A randomized control trial. Jama. 1988;260:2537–41.

] <ref-linzer1988impact>
#block[
22. Horsley T, Hyde C, Santesso N, Parkes J, Milne R, Stewart R. Teaching critical appraisal skills in healthcare settings. Cochrane Database of Systematic Reviews. 2011.

] <ref-horsley2011teaching>
#block[
23. Mimura T, Kondo Y, Ohta A, Iwamoto M, Ota A, Okamoto N, et al. #link("https://doi.org/10.1080/14397595.2018.1465633")[Evidence-based clinical practice guideline for adult Still’s disease];. Modern Rheumatology. 2018;28:736–57.

] <ref-mimura2018>
#block[
24. Karbach U, Schubert I, Hagemeister J, Ernstmann N, Pfaff H, Höpp H-W. Physicians’ knowledge of and compliance with guidelines: An exploratory study in cardiovascular diseases. Deutsches Ärzteblatt International. 2011;108:61.

] <ref-karbach2011physicians>
#block[
25. Farmer AP, Legare F, Turcot L, Grimshaw J, Harvey E, McGowan J, et al. Printed educational materials: Effects on professional practice and health care outcomes. Cochrane Database of Systematic Reviews. 2008.

] <ref-farmer2008printed>
#block[
26. McGowan J, Grad R, Pluye P, Hannes K, Deane K, Labrecque M, et al. Electronic retrieval of health information by healthcare providers to improve practice and patient care. Cochrane Database of Systematic Reviews. 2009.

] <ref-mcgowan2009electronic>
#block[
27. Pakenham-Walsh N, Priestley C, Smith R. Meeting the information needs of health workers in developing countries: A new programme to coordinate and advise. Bmj. 1997;314:90.

] <ref-pakenham1997meeting>
#block[
28. Khan KS, Coomarasamy A. A hierarchy of effective teaching and learning to acquire competence in evidenced-based medicine. BMC medical education. 2006;6:1–9.

] <ref-khan2006hierarchy>
#block[
29. Sepucha KR, Belkora JK, Mutchnick S, Esserman LJ. Consultation planning to help breast cancer patients prepare for medical consultations: Effect on communication and satisfaction for patients and physicians. Journal of clinical oncology. 2002;20:2695–700.

] <ref-sepucha2002consultation>
#block[
30. Goffredo Freddi, José Luis Romàn-Pumar. #link("https://doi.org/10.4415/ANN_11_01_06")[Evidence-based medicine: what it can and cannot do];. Annali dell’Istituto Superiore di Sanità. 2010;47.

] <ref-goffredofreddi2010>
] <refs>
== Declarations
<declarations>
=== Ethics approval and consent to participate
<ethics-approval-and-consent-to-participate>
Vestibulum ultrices, tortor at mattis porta, odio nisi rutrum nulla, sit amet tincidunt eros quam facilisis tellus. Fusce eleifend lectus in elementum lacinia. Nam auctor nunc in massa ullamcorper, sit amet auctor ante accumsan. Nam ut varius metus. Curabitur eget tristique leo. Cras finibus euismod erat eget elementum. Integer vel placerat ex. Ut id eros quis lectus lacinia venenatis hendrerit vel ante.

=== Consent for publication
<consent-for-publication>
Not applicable

=== Availability of data and materials
<availability-of-data-and-materials>
The original data that support the findings of this study are available from Open Science Framework \(osf.io, see manuscript-URL).

=== Competing interests
<competing-interests>
The authors declare that they have no competing interests.

=== Funding
<funding>
The author\(s) received no specific funding for this work.

=== Authors’ contributions
<authors-contributions>
Nulla eget cursus ipsum. Vivamus porttitor leo diam, sed volutpat lectus facilisis sit amet. Maecenas et pulvinar metus. Ut at dignissim tellus. In in tincidunt elit. Etiam vulputate lobortis arcu, vel faucibus leo lobortis ac. Aliquam erat volutpat. In interdum orci ac est euismod euismod. Nunc eleifend tristique risus, at lacinia odio commodo in. Sed aliquet ligula odio, sed tempor neque ultricies sit amet.

=== CRediT authorship contribution statement
<credit-authorship-contribution-statement>
#strong[Wolf Jonas Friederichs:] Conceptualization, Data curation, Formal analysis, Investigation, Methodology, Visualization, Writing - original draft. \
#strong[Hendrik Friederichs:] Formal analysis, Investigation, Methodology, Supervision, Writing - review & editing, Writing - original draft.

=== Acknowledgments
<acknowledgments>
The authors are grateful for the insightful comments offered by the anonymous peer reviewers at Ziel-Journal. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.
