---
bibliography: text/references.bib
filters:
  - critic-markup
---

### Broad problem

Evidence-Based Medicine (EBM) provides a structured approach for integrating the latest research findings into clinical decision-making. This integration is fundamental for enhancing patient care.Sackett @sackett1996evidence described in his seminal article the necessity to align all available evidence directly to the individual patient's context. Djulbegovic and Guyatt (2017) emphasize that EBM grounds medical practice in solid scientific research, considering patient preferences and values in decision-making @djulbegovic2017. Therefore, the implementation of EBM in clinical practice is crucial for ensuring high-quality patient care.

### Theoretical and/or empirical focus of the problem

While EBM is widely recognized, its consistent application in clinical practice remains challenging. Research integration into clinical practice is slow @berwick2003disseminating. For instance, in plastic and reconstructive surgery, surgeons often over-prescribe antibiotic prophylaxis despite evidence for specific indications and durations @klifto2023. Integrating even clearly effective innovations into daily clinical practice proves difficult @hayward2005sins.

Green and colleagues interviewed junior doctors about emerging questions during patient contacts @green2005residents. They found two new questions per three patients, mainly regarding therapeutic (38%) or diagnostic (27%) issues, with an average of 0.7 questions per contact. Although junior doctors deemed 70% as crucial for patient treatment, only 29% of these questions were pursued. Similar investigation rates were found by Gorman (14 % on the same day and of 30 % within five days) @gorman1995information and Covell (30 % within the same day) @covell1985information. The primary reasons for not pursuing questions were lack of time (60%) and forgetting the question (29%) @green2000residents.

Thus, several problems occur in daily clinical practice:

#### Time

A systematic review identified time constraint as the primary barrier for junior doctors practicing evidence-based medicine (EBM) @van2010barriers. In clinical practice, medical searches must be concise. Research indicates that experienced researchers require 45 minutes to locate all relevant original articles @gorman1994can. Sackett and Straus report that doctors on ward rounds typically spend about 90 seconds per search @sackett1998finding.

#### Search

The initial step in evidence-based decision-making involves transforming medical information into specific clinical questions. On average, searches take 15 minutes, primarily utilizing textbooks (31%) and original articles (21%) (QUELLE?). Ho and colleagues noted that only 20% of junior doctors find implementing EBM straightforward @ho2006virtual, while Montori et al. identified (non) existing expertise as a key issue @montori2002qualitative.

#### Language

Language barriers, especially in reading and understanding English-language original articles, are significant in countries like Germany, Hungary, and Poland, more so than in the Netherlands or Switzerland @oude2011can. This barrier also affects Japanese and other evidence-based medicine teachers @matsui2004poor; @letelier2007exploring.

#### Teaching

Bridging the gap between EBM and clinical practice may be achieved through targeted education and training programs. Despite efforts, no positive effect of post-graduate EBM training on medical education has been proven as of mid-2010 @feldstein2010evidence; @flores2007evidence. Clinically integrated courses are known to positively affect EBM skills, attitudes, and behavior @coomarasamy2004evidence. Critical appraisal teaching, a cornerstone of EBM, involves evaluating and interpreting evidence as well as a systematic integration of the validity, the results and the relevance of the study for the own individual work. Yet only one high-quality study @linzer1988impact demonstrated its positive impact on doctors' skills and knowledge @horsley2011teaching.

Effective strategies are needed to enhance EBM application in daily practice. Mimura et al. (2018) emphasized the importance of developing and disseminating EBM-based clinical practice guidelines @mimura2018. However, guideline knowledge among physicians does not necessarily translate into better practice, as shown in a study of 2,500 general practitioners @karbach2011physicians. Farmer et al. found that distributing research results and guidelines has a minimal positive impact on clinical work @farmer2008printed.

Identifying barriers and facilitators for integrating EBM into post-graduate training is crucial. A survey among EBM-teachers highlighted time constraints as a major obstacle @oude2011can. Green and Ruff suggest mobile devices and specially designed web pages to aid junior doctors in their research @green2005residents. However, there is no convincing evidence that teaching interventions regarding digital medical data access affect clinical practice or patient outcomes @mcgowan2009electronic. Consequently, Green and Ruff propose the use of mobile devices to solve the problems of lack of time and of computer access. They also recommend specially designed web pages supporting junior doctors in their search @green2005residents. Probably the access to the global knowledge is the most cost-effective and accessible strategy to improve healthcare @pakenham1997meeting.

### Focused problem statement

The discrepancy between the theoretical ideal and practical implementation of EBM is evident. Despite EBM's potential to improve healthcare, its consistent application is not always realized. The best way to enhance evidence-based behavior in clinical practice is through the integration of EBM into clinical workflows @khan2006hierarchy. Technological advancements have significantly improved access to digital data. Up to now, access to the internet has spread markedly and led to a much better accessibility to global knowledge. Also, technical development increased the options to access digital data drastically. For example, it is possible to search medical databases via the internet and mobile devices within seconds. Original works increasingly are read directly or downloaded, too. For instance, PubMed publishes annually 40,000 studies (2010), with a rising trend. Databases like the Cochrane Library and ACP Journal Club, and various clinical guideline portals, focus on clinical issues and aid in treatment recommendations.

### Statement of study intent

We propose developing a mobile device application to facilitate access to medical knowledge in everyday clinical settings. This application will transform the Apple Watch into a sophisticated portal for medical knowledge, leveraging PubMed's extensive database. It will enable users to quickly access medical information through voice commands, simplifying complex data retrieval. This tool aims to enhance efficiency for medical professionals and researchers by providing instantaneous and intuitive access to medical research.

The application encompasses the following components:

1.  **Voice Command for Query Input** Users can initiate searches through voice commands, streamlining the information retrieval process.

2.  **PubMed Search** PubMed, the most respected source for medical literature, serves as the primary database. The challenge lies in effectively filtering search results to capture the most relevant and recent studies.

3.  **Abstract Summarization via TLDRs from SemanticScholar or GPT-4 Large Language Model** Summaries generated by TLDRs from SemanticScholar or GPT-4 must be meticulously verified for accuracy. This ensures that the condensed information remains reliable and useful for medical professionals.

We performed a study of medical students to investigate the following questions:

1.  What is ...
2.  Why are ...
