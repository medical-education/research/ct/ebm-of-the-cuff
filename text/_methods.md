
### Setting and subjects

...

### Study design

...

### Ethical approval

{{< lipsum 1 >}}

### Data collection

Data collection for this study was determined à priori as follows:

...

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

### Outcome Measures

...

### Statistical methods

...

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # Download the dataset

# Read the data
penguins = read.csv("penguins.csv")

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # Build a scatterplot
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species),
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4"))
```

