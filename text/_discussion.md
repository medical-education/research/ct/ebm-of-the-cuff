---
bibliography: text/references.bib
---

### Summary

After the evaluation of all datasets, the following findings emerged. The first is that ...

### Limitation: study population

{{< lipsum 1 >}}

### Limitation: study design

...

### Integration with prior work

According to Sepucha et al., 49 % of breast cancer patients named the flood of information as a communication barrier for doctor-patient-consultations @sepucha2002consultation. In this study by Sepucha, 14 % of the patients expected not to tell their doctor their concerns during the next consultation (genauer-Lesen!) because they were afraid of their doctor not knowing the answer @sepucha2002consultation.

On annual average, physicians affiliated to the German public health care system treat the internationally still very high number of 34 patients per day. To implement all recommended measures for an appropriate prevention and care of his chronically ill patients, a general practitioner would need about 18 hours daily. Partly for this reason, on average, less than half of an evidence-based care is currently realized (Gesundheitsweise 2009 – QUELLE).

On the other hand, junior doctors consider the discussion with the patients about existing evidence and the integration of the patient’s opinion into the treatment decision a preferred style of work @montori2002qualitative.

### Implications for practice

Freddi und Román-Pumar (2011) diskutieren, wie EBM manchmal als Einschränkung der klinischen Freiheit und als Hemmnis für Innovationen wahrgenommen wird @goffredofreddi2010. Diese Wahrnehmung kann die Umsetzung von EBM in der Praxis beeinträchtigen und unterstreicht die Notwendigkeit, diese Lücke zu schließen.

### Implications for research

{{< lipsum 1 >}}

### Conclusions

...
