
plain-language-summary: |
  **Relevantes Problem:** Evidence-Based Medicine (EBM) bietet einen strukturierten Ansatz, um neueste Forschungsergebnisse in klinische Entscheidungen einzubeziehen, wobei individuelle Patientenpräferenzen berücksichtigt werden. Ihre Implementierung ist entscheidend für eine qualitativ hochwertige Patientenversorgung, allerdings bleibt die Integration von EBM in den klinischen Alltag eine Herausforderung.
  
  **Fokussiertes Problem:** Die konsequente Anwendung von EBM in der Praxis stößt auf Schwierigkeiten. Beispielsweise zeigen Studien in verschiedenen medizinischen Bereichen eine zögerliche Integration effektiver Innovationen. Häufige Probleme umfassen Zeitmangel und eine langsame Etablierung neuer Erkenntnisse. Jünge Ärzte stehen vor der Herausforderung, relevante Fragen in der Patientenversorgung effektiv zu recherchieren. Dabei sind Zeitmangel und das Vergessen der klinischen Fragestellung Hauptgründe für eine unzureichende Recherche. Zudem stellt insbesondere in nicht-englischsprachigen Ländern das Verständnis englischer Fachliteratur eine Hürde dar. Auch die Ausbildung in EBM an medizinischen Fakultäten ist verbesserungswürdig, da nur klinisch integrierte Kurse einen positiven Effekt auf EBM-Kompetenzen zeigen.
  
  **Gap des Problems:** 
  <!-- Gap / Dilemma / Widerspruch subj. Erwartung und Realität -->
  Es existiert eine Lücke zwischen dem theoretischen Ideal von EBM und dessen praktischer Umsetzung. Der Wissenstransfer über Fachzeitschriften und Leitlinien reicht nicht aus, um eine effektive Anwendung von EBM in der täglichen Praxis zu gewährleisten.
  
  **Lösung?:** 
  <!-- möglicher Fortschritt / mögliche Lösung - Ansprechen von Motiv(en): Anschluss, Leistung, Macht -->
  Die Nutzung mobiler Geräte und spezieller Webseiten könnte helfen, Zeitmangel und Zugangsprobleme zu lösen. Trotz der Verfügbarkeit digitaler Ressourcen gibt es jedoch keine überzeugenden Beweise dafür, dass Lehrinterventionen zur Nutzung digitaler medizinischer Daten einen Einfluss auf die klinische Praxis haben. Die Studie zielt darauf ab, eine mobile App zu entwickeln, die den Zugang zu medizinischem Wissen im klinischen Alltag erleichtert. Die App, die für die Apple Watch konzipiert ist, ermöglicht es, über Sprachbefehle schnell und intuitiv auf medizinische Forschungsergebnisse zuzugreifen und soll die Effizienz für Mediziner erhöhen.
   
  **Forschungsfragen:** 
  Daher wurde eine Studie durchgeführt, um ...
  
  **Studienpopulation:** ...
  
  **Studiendesign:** ...  
  
  **Datenerhebung:**  ...  
  
  **Ergebnisparameter:**  Anzahl der richtigen Antworten insgesamt.
  
  **Statistik:**  Bestimmung der Prozentwerte für die absolute und relative Bewertung der Leistungen.




